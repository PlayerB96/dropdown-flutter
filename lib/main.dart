import 'package:dropdown/views/FirstRoute.dart';
import 'package:dropdown/views/my_home_page.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return VRouter(
      title: 'LocatorApp',
      debugShowCheckedModeBanner: false,
      initialUrl: '/homepage',
      routes: [
        VWidget(
            path: '/homepage',
            widget: MyHomePage(),
            stackedRoutes: [
              VWidget(
                path: '/firstroute', 
                widget: FirstRoute()
              )
            ]
        )
      ],
    );
  }
}
